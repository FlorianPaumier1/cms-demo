<form class="form-container" action="{{ action }}" method="{{ method }}">
    {{ form_view }}
</form>