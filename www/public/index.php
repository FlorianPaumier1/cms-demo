<?php

use App\Core\Kernel\Kernel;
use App\Core\Router\Router;

define("ROOT_PATH", dirname(__DIR__, 1));

require "../src/Core/Helpers/autoloader.php";

$kernel = new Kernel();
$kernel->resolve();