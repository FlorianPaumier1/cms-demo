<?php
namespace Tests\Framework\Database;

use App\Core\Database\Table;
use PDO;
use PHPUnit\Framework\TestCase;

class TableTest extends TestCase{

    /**
     * @var Table
     */
    private $table;

    public function setUp(): void
    {
        $pdo = new PDO('sqlite::memory:', null, null, [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ
        ]);
        $pdo->exec('CREATE TABLE test (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name VARCHAR(255),
            firstname VARCHAR(255)
        )');

        $this->table = new Table($pdo);
        $this->table->table("test");
        $reflection = new \ReflectionClass($this->table);
        $property = $reflection->getProperty('from');
        $property->setAccessible(true);
        $property->setValue($this->table, 'test');
    }


    public function testFind()
    {
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES ("a1")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES ("a2")');
        $this->table->setEntity(\stdClass::class);
        $test = $this->table->find(1);
        $this->assertInstanceOf(\stdClass::class, $test);
        $this->assertEquals('a1', $test->name);
    }

    public function testFindList()
    {
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES ("a1")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES ("a2")');
        $this->assertEquals(['1' => 'a1', '2' => 'a2'], $this->table->findList());
    }

    public function testFindAll()
    {
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES ("a1")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES ("a2")');
        $categories = $this->table->findAll();
        $this->table->setEntity(\stdClass::class);
        $this->assertCount(2, $categories);
        $this->assertInstanceOf(\stdClass::class, $categories[0]);
        $this->assertEquals('a1', $categories[0]->name);
        $this->assertEquals('a2', $categories[1]->name);
    }

    public function testFindBy()
    {
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES ("a1")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES ("a2")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES ("a3")');
        $this->table->setEntity(\stdClass::class);
        $category = $this->table->findBy(['name' => 'a1']);
        $this->assertInstanceOf(\stdClass::class, $category);
        $this->assertEquals(1, (int)$category->id);
    }

    public function testExists()
    {
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES ("a1")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES ("a2")');
        $this->table->setEntity(\stdClass::class);
        $this->assertTrue($this->table->exists(1));
        $this->assertTrue($this->table->exists(2));
        $this->assertFalse($this->table->exists(3123));
    }

    public function testCount()
    {
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES ("a1")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES ("a2")');
        $this->table->getPdo()->exec('INSERT INTO test (name) VALUES ("a1")');

        $this->assertEquals(3, $this->table->count());
    }

    public function testInsert()
    {
        $this->table->setEntity(\stdClass::class);
        $this->table->insert(['name' => 'Salut', 'firstname' => 'demo']);
        $post = $this->table->find(1);
        $this->assertEquals('Salut', $post->name);
        $this->assertEquals('demo', $post->firstname);
    }

    public function testUpdate()
    {
        $this->table->setEntity(\stdClass::class);
        $this->table->insert(['name' => 'Salut', 'firstname' => 'demo']);
        $this->table->update(1, ['name' => 'Salut', 'firstname' => 'demo']);
        $post = $this->table->find(1);
        $this->assertEquals('Salut', $post->name);
        $this->assertEquals('demo', $post->firstname);
    }

    public function testDelete()
    {
        $this->table->insert(['name' => 'Salut', 'firstname' => 'demo']);
        $this->table->insert(['name' => 'Salut', 'firstname' => 'demo']);
        $count = $this->table->getPdo()->query('SELECT COUNT(id) FROM test')->fetchColumn();
        $this->assertEquals(2, (int)$count);
        $this->table->delete($this->table->getPdo()->lastInsertId());
        $count = $this->table->getPdo()->query('SELECT COUNT(id) FROM test')->fetchColumn();
        $this->assertEquals(1, (int)$count);
    }

}