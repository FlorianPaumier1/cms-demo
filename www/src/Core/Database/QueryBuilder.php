<?php
namespace App\Core\Database;

use PDO;

class QueryBuilder
{

    /** @var \PDO */
    protected $pdo;

    /** @var array */
    protected $select = [];

    /** @var string */
    protected $from;

    /** @var array */
    protected $order = [];

    /** @var string */
    protected $sql;

    /** @var int */
    protected $limit;

    /** @var string */
    protected $where;

    /** @var array */
    protected $andWhere = [];

    /** @var array */
    protected $fields = [];
    /**
     * @var int
     */
    protected $offset;

    /**
     * @var string
     */
    protected $method = "SELECT";

    public function __construct(\PDO $PDO = null)
    {
        if(is_null($PDO)) {
            $PDO = Connection::instance();
        }

        $this->pdo = $PDO;
    }

    public function getPdo(){
        return $this->pdo;
    }

    public function table(string $table, string $alias = null): self
    {
        $this->from = $table;
        if(!is_null($alias)){
            $this->from .= " $alias";
        }

        return $this;
    }

    public function orderBy(string $target, string $sort){
        $sort = strtoupper($sort);
        if(!in_array($sort, ["ASC", "DESC"])){
            $this->order[] = $target;
        }else{
            $this->order[] = "$target $sort";
        }

        return $this;
    }

    public function limit(int $length){
        $this->limit = $length;
        return $this;
    }

    public function where(string $target)
    {
        $this->where = $target;
        return $this;
    }

    public function andWhere(string $target)
    {
        $this->andWhere[] = $target;
        return $this;
    }

    public function offset(int $offset)
    {
        $this->offset = $offset;
        return $this;
    }

    public function page(int $page)
    {
        if (is_null($this->limit)){
            throw new \Exception("Le paramètre limit doit être renseigné", 500);
        }

        return $this->offset($this->limit * ($page - 1));
    }

    public function select(...$fields)
    {
        $this->method = "SELECT";

        if (is_array($fields[0])) {
            $fields = $fields[0];
        }


        if ($this->select === ['*']) {
            $this->select = $fields;
        } else {
            $this->select = array_merge($this->select, $fields);
        }

        return $this;
    }

    public function setParameters(string $key, $value){
        $this->fields[$key] = $value;
        return $this;
    }

    public function toSQL(){

        if($this->method === "SELECT"){
            $this->sql = "$this->method ";
            if(!empty($this->select)){
                $this->sql .= implode(", ", $this->select);
            }else{
                $this->sql .= "*";
            }

            $this->sql .= " FROM $this->from";
        }

        if ($this->where) {
            $this->sql .= " WHERE " . $this->where;
        }

        if (count($this->andWhere) > 0) {
            foreach ($this->andWhere as $selector) {
                $this->sql .= " AND WHERE " . $selector;
            }
        }

        if(!empty($this->order)){
            $this->sql .= " ORDER BY ".implode(", ", $this->order);
        }

        if ($this->limit > 0) {
            $this->sql .= " LIMIT " . $this->limit;
        }

        if ($this->offset !== null) {
            $this->sql .= " OFFSET " . $this->offset;
        }


        return $this->sql;
    }

    /**
     * @param  PDO $pdo
     * @return int
     */
    public function count(): int
    {
        $q = (clone $this)->select('COUNT(id) count')->fetchOrFail('count');

        if(is_array($q)) {
            $count = (int)$q["count"];
        }else{
            $count = (int)$q;
        }

        return $count;
    }


    protected function fetchAll(int $type)
    {
        return $this->pdo->prepare($this->toSQL())
            ->fetchAll($type);
    }

    public function fetchOrFail(string $field = null){
        $query = $this->pdo->prepare($this->toSQL());
        $query->execute($this->fields);

        if(!is_null($field)){
            $result = $query->fetchColumn();
            if ($result === false) {
                return null;
            }

            return $result[$field] ?? null;
        }

        if (isset($this->entity)) {
            $query->setFetchMode(\PDO::FETCH_CLASS, $this->entity);
            return $query->fetchObject();
        }

        $record = $query->fetchAll();

        return $record !== false ? $record : null;
    }

    public function __destruct()
    {
        foreach (get_class_vars(get_class($this)) as $var => $def_val){
            $this->$var= $def_val;
        }
    }
}