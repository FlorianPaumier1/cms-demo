<?php
namespace App\Core\Database;

class Table extends QueryBuilder
{
    /**
     * Entité à utiliser
     *
     * @var string|null
     */
    protected $entity;

    public function __construct(\PDO $PDO = null)
    {
        parent::__construct($PDO);
    }

    public function find(int $id)
    {
        return $this->where("id = :id")
            ->setParameters("id", $id)
            ->fetchOrFail();
    }

    public function findList()
    {
        $results = $this->select("id", "name")
            ->table($this->from)
            ->fetchAll(\PDO::FETCH_NUM);

        $list = [];
        foreach ($results as $result) {
            $list[$result[0]] = $result[1];
        }
        return $list;
    }

    public function findAll()
    {
        $query = $this->table($this->from);
        return $query->fetchOrFail();
    }

    public function findBy(array $criteres)
    {
        $index = 0;

        foreach ($criteres as $field => $critere){
            if($index === 0){
                $this->where("$field = :$field");
                $this->setParameters($field, $critere);
            }else{
                $this->andWhere("$field = :$field");
                $this->setParameters($field, $critere);
            }
            $index++;
        }

        return $this->fetchOrFail();
    }

    public function setEntity(string $class)
    {
        $this->entity = $class;
    }

    public function exists(int $id)
    {
        $sql = $this->where("id = :id")
            ->setParameters("id" , $id)
            ->toSQL();

        $query = $this->pdo->prepare($sql);
        $query->execute();

        return $query->fetchColumn() !== false;
    }

    public function insert(array $params)
    {
        $query = $this->pdo->prepare(
            "INSERT INTO {$this->from} ("
            . join(', ', array_keys($params)) .
            ") VALUES (" . $this->buildFieldQuery($params) . ")"
        );


        if (!($result = $query->execute($params))) {
            foreach ($params as $key => $param) {
                $query->bindParam($key, $param);
            }
            $result = $query->execute($params);
        }

        return $result;
    }

    /**
     * Supprime un enregistrment
     *
     * @param  int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        $query = $this->pdo->prepare("DELETE FROM {$this->from} WHERE id = :id");
        return $query->execute(["id" => $id]);
    }


    /**
     * Met à jour un enregistrement au niveau de la base de données
     *
     * @param  int   $id
     * @param  array $params
     * @return bool
     */
    public function update(int $id, array $params): bool
    {
        $fieldQuery = implode(
            ',', array_map(
                function ($param) {
                    return $param . "=:" . $param;
                }, array_keys($params)
            )
        );

        $params["id"] = $id;
        $query = $this->pdo->prepare("UPDATE {$this->from} SET {$fieldQuery} WHERE id = :id");
        $query->execute($params);
        return $query->fetchObject();
    }

    private function buildFieldQuery(array $params)
    {
        return join(
            ', ', array_map(
                function ($field) {
                    return ":$field";
                }, array_keys($params)
            )
        );
    }
}