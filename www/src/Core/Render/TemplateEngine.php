<?php


namespace App\Core\Render;


use App\Core\Cache\CacheHandler;

class TemplateEngine
{
    private const EXT_ALLOWED = [
        'view.php',
        'tpl.php',
        'modal.php',
    ];

    /**
     * @var string
     */
    private $template;

    /** @var string */
    private $view;

    /** @var string */
    private $content;

    /**
     * @var ParseView
     */
    private $parseView;
    private $templateContent;

    /** @var array */
    private $methodEntries;

    /** @var array */
    private $fieldEntries;
    /**
     * @var array
     */
    private $data;

    /** @var array */
    private $extendMethod;

    /** @var array */
    private $blockEntries;

    public function __construct(string $template = "back")
    {
        $this->parseView = new ParseView();
    }

    public function setView(string $view, bool $absolute = false)
    {

        if ($absolute && file_exists($view)) {
            $this->content = file_get_contents($view);
        } else {
            if (!($this->view = $this->fileExist($view))) {
                throw new \Exception("Page $view not Found", 404);
            }

            $this->content = file_get_contents(VIEW_PATH . "/$view");
        }

        return true;
    }

    private function fileExist($view)
    {
        return (in_array(explode(".", $view, 2)[1], self::EXT_ALLOWED) && file_exists(VIEW_PATH . "/$view")) ? VIEW_PATH . "/$view" : false;
    }

    public function generateMarkup()
    {
        if (!is_null($view = CacheHandler::get($this->view))) {
            $this->content = $view;
        } else {
            $this->_extendFile();
            #Todo : Add Extend File Function
        }
    }

    private function _extendFile()
    {
        // Get the path to the view to extend
        preg_match_all("#^@extend\(([\w+\/?]+)\)$#m", $this->content, $this->extendMethod, PREG_SET_ORDER, 0);

        if (is_null($this->extendMethod)) {
            return "";
        }

        foreach ($this->extendMethod as $tag) {
            $value = $tag[1];
            $html = null;
            $this->content = RenderFunction::extend($value, $this->content);
        }
    }

    public function compile(bool $tags = true)
    {

        extract(["view" => $this->view]);
        $this->_parseTemplate();

        if (count($this->methodEntries) > 0) {
            $this->_parseFunctions();
        }

        if ($tags) {
            CacheHandler::set($this->view, $this->content);

            if (count($this->fieldEntries) > 0) {
                $this->_parseTags();
            }

        }

        if(isset($this->data["render"]) && $this->data["render"]){
            return $this->templateContent;
        }

        if (!is_null($this->view)) {
            $tmp = tmpfile();
            fwrite($tmp, $this->content);
            include stream_get_meta_data($tmp)['uri'];

        }
    }

    private function _parseTemplate()
    {
        preg_match_all("/{{\s?(\w+)\s?}}/m", $this->content, $this->fieldEntries, PREG_SET_ORDER, 0);
        preg_match_all("/{{ (@?[\w+\/?]+)\|(\w+) }}/m", $this->content, $this->methodEntries, PREG_SET_ORDER, 0);
    }

    private function _parseFunctions()
    {
        #Todo implement function
    }

    private function _parseTags()
    {
        $this->content = $this->parseView->parseTags($this->content, $this->data, $this->fieldEntries);
    }

    public function setData(array $args)
    {
        $this->data = $args;
    }

    public function createHtmlElement(string $type)
    {
        preg_match_all("/.*{%\s?block\s${type}\s?%}((?s:.+?)){%\s?endblock\s?%}.*/m", $this->content, $this->blockEntries, PREG_SET_ORDER, 0);

        $this->content = $this->blockEntries[0][1] ?? "";
        $this->_compileForm();
        return $this->content;
    }

    private function _compileForm()
    {
        $this->_parseTemplate();
        $this->_parseTags();
    }
}