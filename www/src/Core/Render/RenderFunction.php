<?php


namespace App\Core\Render;


class RenderFunction
{

    public static function extend(string $value, string $content)
    {
        if(file_exists(VIEW_PATH."/$value.tpl.php")) {
            $extendFile = file_get_contents(VIEW_PATH."/$value.tpl.php");
        }else{
            throw new \Exception("Page not found", 404);
        }

        preg_match_all("#^\s*?@section\((\w+)\)$#m", $extendFile, $sections);

        foreach ($sections[1] as $key => $value){
            preg_match("#^\s*?@section\($value\)([\s\S]*)@endsection$#mU", $content, $sectionsValue);
            $extendFile = preg_replace("#^\s*?@section\($value\)$#m", $sectionsValue[1], $extendFile);
        }

        return $extendFile;
    }
}