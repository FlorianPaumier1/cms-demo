<?php
namespace App\Core\Render;

class RenderView
{

    /**
     * @var
     */
    private $templateEngine;

    /**
     * Data to inject into view
     * @var array
     */
    private $data = [];

    public function __construct()
    {
        $this->templateEngine = new TemplateEngine();
    }

    public function render(string $view, array $args = [], bool $render = false)
    {
        $viewAdd = $this->templateEngine->setView($view);

        if($viewAdd){
            $this->templateEngine->setData(["parameter" => $args, "render" => $render]);
            $this->templateEngine->generateMarkup();
        }

        if($render){
            return $this->compile();
        }
        return $this;
    }

    public function setData($data){
        $this->data["parameters"][] = $data;
    }

    public function compile()
    {
        return $this->templateEngine->compile();
    }

    public function renderHtmlElement(string $type, array $args)
    {
        $this->templateEngine->setData(["parameter" => $args]);
        $this->templateEngine->setView(dirname(__DIR__)."/Form/form.tpl.php", true);
        return $this->templateEngine->createHtmlElement($type);
    }
}