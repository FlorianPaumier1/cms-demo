<?php


namespace App\Core\Render;


class ParseView
{

    public function parseTags(string $templateContent, array $data, array $fieldEntries)
    {

        foreach ($data["parameter"] as $key => $value) {
            foreach ($fieldEntries as $tag) {
                if ($tag[1] == $key) {
                    $templateContent = preg_replace("/{{\s?($key)\s?}}/", $value, $templateContent);
                }
            }
        }

        return $templateContent;
    }
}