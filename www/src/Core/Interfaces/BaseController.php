<?php

namespace App\Core\Interfaces;

use App\Core\Kernel\Kernel;
use App\Core\Render\RenderView;
use App\Core\Request\Request;

abstract class BaseController
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * #todo: Faire un Template Engine
     * @var
     */
    protected $render;

    /**
     * #TODO : Faire notre ORM
     * @var
     */
    protected $entityManager;

    /**
     * @var
     */
    protected $session;

    public function __construct()
    {
        $this->request = Kernel::get('request');
        $this->render = new RenderView();
    }
}