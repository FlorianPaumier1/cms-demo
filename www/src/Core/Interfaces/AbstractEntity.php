<?php


namespace App\Core\Interfaces;


use App\Core\database\Hydrator;
use App\Core\Database\Table;
use App\Core\Kernel\Kernel;

class AbstractEntity
{
    private $id;

    public function setId(?int $id){
        $this->id = $id;

        if(is_null($id)){
            return $this;
        }

        /** @var Table $table */
        $table = Kernel::get('repository');
        $entityNamespace = explode("\\", get_class());
        $entityName = strtolower(array_pop($entityNamespace));
        return Hydrator::hydrate($table->table($entityName)->find($id), $this);
    }

    public function getVariables()
    {
        return get_object_vars($this);
    }

}