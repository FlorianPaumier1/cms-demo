<?php


namespace App\Core\Request;


class ParameterBag
{

    /** @var array */
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function get($key): ?string {
        if(isset($this->data[$key])){
            return $this->data[$key];
        }

        return null;
    }

    public function all(){
        return $this->data;
    }
}