<?php


namespace App\Core\Request;


class RequestHandler
{
    public const METHOD_ALLOWED = [
        "POST",
        "GET",
        "PUT",
        "DELETE"
    ];

    public static function handleRequestFromGlobals(){
        return (new Request())
        ->setQuery(new ParameterBag($_GET))
        ->setRequest(new ParameterBag($_POST))
        ->setCookies($_COOKIE)
        ->setFiles($_FILES)
        ->setServer($_SERVER)
        ->setContentType($_SERVER["CONTENT_TYPE"] ?? null);
    }
}