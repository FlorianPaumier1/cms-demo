<?php

namespace App\Core\Request;

class Request
{

    private $query;
    private $request;
    private $attributes;
    private $cookies;
    private $files;
    private $server;
    private $content;

    public function setRequest($data): self
    {
        $this->request = $data;
        return $this;
    }

    public function setQuery($data): self
    {
        $this->query = $data;
        return $this;
    }

    public function setServer($data): self
    {
        $this->server = $data;
        return $this;
    }

    public function setCookies($data): self
    {
        $this->cookies = $data;
        return $this;
    }

    public function setFiles($data): self
    {
        $this->files = $data;
        return $this;
    }

    public function setContentType(?string $contentType): self
    {
        $this->content = $contentType;
        return $this;
    }

    public function get(string $key)
    {
        $property = mb_strtolower($key);
        if(isset($this->$property)){
            return $this->$property;
        }

        throw new \Exception("the property $key don't exist", 404);
    }

    public function getMethod()
    {
        return $this->server["REQUEST_METHOD"] ?? "GET";
    }
}