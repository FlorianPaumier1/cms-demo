<?php


namespace App\Core\Form;


interface FormTypeInterface
{
    /**
     * Function to get the validation rules for the form
     */
    public static function getConstraint(): array;

    /**
     * @return mixed
     * Create the input to show
     */
    public function buildForm(): FormBuilder;

    /**
     * Set prefix for input
     */
    public function setBlockPrefix(): void;
    public function getBlockPrefix(): string;
}