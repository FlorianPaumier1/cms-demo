<?php


namespace App\Core\Form;


use App\Core\Render\RenderView;

class FormBuilder
{

    /** @var string  */
    protected $prefix = "app";

    /** @var array */
    private $fields = [];

    /** @var string  */
    private $action = "";

    /** @var RenderView */
    private $render;

    /** @var array */
    private $form;

    /** @var array */
    private $data = [];

    /** @var array */
    private $errors = [];

    public function __construct()
    {
        $this->render = new RenderView();
    }

    public function setAction(string $action): self
    {
        $this->action = $action;
        return $this;
    }

    public function input(string $key, array $args): self
    {
        $value = $this->getValue($key);

        $args = array_merge($args,[
            "value" => $value,
            "key" => $key,
            "errors" => $this->errors,
            "prefix" => $this->prefix
        ]);

        $this->form[$key] = $this->render->renderHtmlElement("input", $args);
        return $this;
    }

    public function button(string $key, array $args)
    {
        $args = array_merge($args,[
            "key" => $key,
            "prefix" => $this->prefix
        ]);

        $this->form[$key] = $this->render->renderHtmlElement("button", $args);
        return $this;
    }

    public function getValue(string $key)
    {
        if (is_array($this->data)) {
            return $this->data[$key] ?? null;
        }

        $method = 'get' . str_replace(' ', '', ucwords(str_replace('_', ' ', $key)));
        $value = $this->data->$method();
        if ($value instanceof \DateTimeInterface) {
            return $value->format('Y-m-d H:i:s');
        }
        return $value;
    }

    public function createView(){
        $html =  implode(
            "", array_map(
                function ($input) {
                    return html_entity_decode($input);
                }, $this->form
            )
        );

        return $this->render->render("templates/form.tpl.php", [
            "form_view" => $html,
            "method" => "POST",
            "action"  => $this->action,
        ], true);
    }

    public function getBlockPrefix(): string{
        return $this->prefix;
    }
}
