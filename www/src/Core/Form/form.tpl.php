{% block input %}
<div class="form-group">
    <label for="{{prefix}}_{{key}}">{{label}}</label>
    <input type="{{type}}"
           id="{{prefix}}_{{key}}"
           class="{{class}} " placeholder="{{label}}"
           name="{{prefix}}_{{key}}" value="{{value}}" {{ required|isRequired }}>
</div>
{% endblock %}

{% block textarea %}
<div class="form-group">
    <label for="{{prefix}}_{{key}}">{{label}}</label>
    <textarea type="text" id="{{prefix}}_{{key}}" class="{{class}}" name="{{prefix}}_{{key}}" {{ required|isRequired }}>{{value}}</textarea>
</div>
{% endblock %}

{% block button %}
<button id="{{prefix}}_{{key}}" name="{{prefix}}_{{key}}" type="{{type}}" class="{{class}}">{{label}}</button>
{% endblock %}

{% block select %}
<div class="form-group">
    <label for="{{prefix}}_{{key}}">{{label}}</label>
    <select id="{{prefix}}_{{key}}" class="{{type}}" name="{{prefix}}_{{key}}[]" {{ required|isRequired }} {{ multi|isMultiple }}>{{optionsHTML}}</select>
</div>
{% endblock %}

{% block errors %}
<div class="invalid-feedback">{{error}}</div>
{% endblock %}

{% block option %}
<option value="{{key}}" {{selected|isSelected}}>{{value}}</option>
{% endblock %}

{% block captcha %}
<input type="text" value="Captcha">
{% endblock %}