<?php


namespace App\Core\Kernel;


use App\Core\Cache\CacheHandler;
use App\Core\Database\Table;

class ConfigHandler
{


    public function __construct()
    {

    }


    public function init()
    {
        define("VIEW_PATH", dirname(__DIR__, 3)."/public/views");
        define("CACHE_PATH",ROOT_PATH."/var/cache");
        define("CONFIG_PATH",ROOT_PATH."/config");
        define("PATH_STYLE", ROOT_PATH. "/public/styles");
        define("PATH_SCRIPT", ROOT_PATH . "/public/scripts");
        define("PATH_LIBRARY", ROOT_PATH . "/node_modules");

        Kernel::bind("repository", new Table());

        $this->getConfig();
        $this->getRouting();
    }

    private function getConfig()
    {
        $config = dirToArray(CONFIG_PATH."/config");

        foreach ($config as $file){
            $name = explode(".", $file)[0];
            Kernel::bind($name, CacheHandler::set($name, yaml_parse_file(CONFIG_PATH."/config/".$file)));
        }
    }

    private function getRouting()
    {
        $routing = [];
        $pathRoute = CONFIG_PATH."/routing";
        foreach (dirToArray(CONFIG_PATH."/routing") as $key => $file) {
            $table = [];
            if(is_int($key)) {
                $table = yaml_parse_file($pathRoute."/$file");
            }else{
                foreach ($file as $route){
                    $dir = dirToArray($pathRoute."/$key");
                    foreach ($dir as $dirFile) {
                        $table = array_merge($table, yaml_parse_file($pathRoute."/$key/$dirFile"));
                    }
                }
            }
            if(!is_null($table)) {$routing = array_merge($routing, $table);
            };
        };

        Kernel::bind("routes", CacheHandler::set("routes", $routing));
    }
}