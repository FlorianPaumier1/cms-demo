<?php

namespace App\Core\Kernel;

use App\Core\Cache\CacheHandler;
use App\Core\Render\RenderView;
use App\Core\Request\Request;
use App\Core\Request\RequestHandler;
use App\Core\Router\Router;
use PHPUnit\Util\Exception;

class Kernel
{
    /** @var array */
    private static $container = [];

    public function __construct(string $env = "dev")
    {

        include_once dirname(__DIR__)."/helpers/VarDumper.php";
        include_once dirname(__DIR__)."/helpers/basicFunction.php";
        (new ConfigHandler())->init();

        $_SERVER["APP_ENV"] = $env;

        self::$container["request"] = RequestHandler::handleRequestFromGlobals();
        self::$container["router"] = new Router();

    }

    /**
     * Retrieve data from the container
     * @param string $key
     * @return mixed
     * @throws \Exception
     */
    public static function get(string $key){
        if(isset(self::$container[$key])){
            return self::$container[$key];
        }else{
            throw new \Exception("The key : $key don't exist in the container", 404);
        }
    }

    /**
     * Store Object to use
     * @param string $key
     * @param $object
     */
    public static function bind(string $key, $object){
        self::$container[$key] = $object;
    }

    public function resolve(){
        $matched = self::$container["router"]->Match();

        if($matched === false){
            throw new Exception("Route not Found", 404);
        }

        /** @var RenderView $response */
        $response = self::$container["router"]->exec();

        if($response){
            $response->compile();
        }else{
            throw new \Exception("La méthode doit retourne une valeur", 500);
        }
    }
}