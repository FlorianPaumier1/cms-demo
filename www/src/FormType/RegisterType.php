<?php


namespace App\FormType;


use App\Core\Form\FormBuilder;
use App\Core\Form\FormTypeInterface;
use App\Core\Kernel\Kernel;

class RegisterType extends FormBuilder implements FormTypeInterface
{

    /**
     * @inheritDoc
     */
    public static function getConstraint(): array
    {
        return [
            "username" => [
                "empty" => false,
                "required" => true,
                "length" => [
                    "min" => 4
                ],
            ],
            "password" => [
                "empty" => false,
                "required" => true,
                "confirmation" => true,
                "length" => [
                    "min" => 8
                ],
            ],
            "email" => [
                "required" => true,
                "empty" => false,
                "unique" => [
                    "table" => "user"
                ]
            ]
        ];
    }

    /**
     * @inheritDoc
     */
    public function buildForm(): FormBuilder
    {

        return $this->setAction(Kernel::get("router")->getPath("admin_login_check"))
            ->input("username",[
                'type' => 'text',
                'required' => true,
                'label' => 'Username',
                "class" => "form-control form-control-user"
            ])
            ->input("email", [
                'type' => 'email',
                'required' => true,
                'label' => 'Email',
                "class" => "form-control form-control-user"
            ])
            ->input('password', [
                'type' => 'password',
                'required' => true,
                'label' => 'Mot de passe',
                "class" => "form-control form-control-user"
            ])
            ->input('password_confirmation',[
                'type' => 'password',
                'required' => true,
                'label' => 'Confirmation du mot de passe',
                "class" => "form-control form-control-user"
            ])
            ->button("submit", [
                "type" => "submit",
                "label" => "Inscription",
                "class" => "btn btn-primary"
            ])
            ;
    }

    /**
     * @inheritDoc
     */
    public function setBlockPrefix(): void
    {
        $this->prefix = "admin_register";
    }

}