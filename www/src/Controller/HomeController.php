<?php
namespace App\Controller;

use App\Core\Interfaces\BaseController;

class HomeController extends BaseController
{
    public function index(){
        return $this->render->render("backend/home.view.php", [
            "home" => "Hello World"
        ]);
    }

    public function home($id){
        dump($id);
    }
}