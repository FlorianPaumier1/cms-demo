<?php

namespace App\Controller\Backend;

use App\Core\Form\Validator;
use App\Core\Interfaces\BaseController;
use App\Entity\UserEntity;
use App\FormType\RegisterType;

class SecurityController extends BaseController
{

    public function login()
    {
        $form = (new RegisterType())->buildForm();
        $view = $this->render->render("backend/security/login.view.php", [
            "form" => $form->createView(),
        ]);

        return $view;
    }

    public function checkLogin()
    {
        $form = new RegisterType();
        if($this->request->getMethod() == "POST"){
            $validator = (new Validator($form))->buildValidator($this->request);

            if($validator->isValid()){
                return $this->render->render("backend/home.view.php");
            }
        }
        die;
        return $this->render->render("backend/security/login.view.php", [
            "form" => ($form->buildForm())->createView(),
        ]);
    }

    public function register()
    {

    }
}