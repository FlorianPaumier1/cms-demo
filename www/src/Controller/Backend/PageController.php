<?php


namespace App\Controller\Backend;


use App\Core\Interfaces\BaseController;

class PageController extends BaseController
{
    public function index()
    {
        $this->render->setView("backend/page.view.php");
    }
}